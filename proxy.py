"""
Usage: py proxy.py [-r REMOTE] [-l LOCAL] [-m MULTI]
Arguments:
    REMOTE            Server IP and Port
    LOCAL             Local IP and Port for clients to connect
    MULTI             Message multiplier
"""

import json
import socket
import time
import argparse
from threading import Thread, RLock
import selectors2 as selectors

parser = argparse.ArgumentParser()
parser.add_argument('--remote', '-r', help="Server IP and Port")
parser.add_argument('--local', '-l', help="Local IP and Port for clients to connect")
parser.add_argument('--multi', '-m', help="Multiplying messages to clients switcher", action="store_true")

args = parser.parse_args()
remote = args.remote
local = args.local
multi = args.multi

r = remote.split(':')
l = local.split(':')


"""Parsing incoming data to get full json package"""
def parsing(data):
    ddata = data
    tmp = ''
    cnt = 0
    steps = 0
    try:
        """Trying to get continuation of json file"""
        try:
            tmp = ddata[1]
        except:
            tmp = ddata[0]
            ddata[0] = ''
        """Trying to get counter"""
        try:
            cnt = ddata[2]
        except:
            cnt = 0
        for i in tmp:
            ddata[0] += i
            steps += 1
            if i == '{':
                cnt += 1
            if i == '}':
                cnt -= 1
            if cnt == 0:
                tmp = tmp[steps:]
                ddata[1] = tmp
                ddata.append(cnt)
                return ddata
        """In case of not completing json"""
        ddata[1] = ''
        ddata.append(cnt)
        return ddata
    except:
        return ddata


"""Merging parsed json packages"""
def merger(conn, data):
    ddata = data
    while int(ddata[2]) != 0:
        recvData = conn.recv(1024)
        ddata[1] = recvData.decode()
        ddata = parsing(ddata)
    res = [ddata[0], ddata[1]]
    return res


def processing(conn, data):
    cnt = 0
    prs = parsing(data)
    res = merger(conn, prs)
    jsonList = []
    jsonList.append(res[0])
    while res[1] != '' and cnt < 100:
        tmp = parsing(['', res[1]])
        res = merger(conn, tmp)
        jsonList.append(res[0])
        cnt += 1
    return jsonList


"""Creating server for clients"""
toClient = socket.socket()
toClient.bind((l[0], int(l[1])))
toClient.listen(128)

"""Constants and variables"""
s = selectors.SelectSelector()
clients = {}
unregClients = {}
lock = RLock()
socket.setdefaulttimeout(0.1)
methodList = ['HandleAuthenticationResult',
              'HandleChannelRegistration',
              'HandleSubscriberRegistration',
              'HandleCallSessionEstablished',
              'HandleCallState']


def listener():
    queue()
    events = s.select(0.1)  # timout 0.1 sec
    try:
        for key, event in events:
            if event and selectors.EVENT_READ:
                try:
                    data = key.fileobj.recv(1024)
                    if data != b'':
                        conn = key.fileobj
                        resList = processing(conn, ['', data.decode()])  # list with full json packages
                        for js in resList:
                            jdata = json.loads(js)
                            method = jdata['method']
                            res = json.dumps(jdata).encode()
                            """Checking if message comes from server"""
                            if key.fileobj in clients.keys():
                                """Checking if multiplying is necessary"""
                                if multi:
                                    for client in clients.values():
                                        client.send(res)
                                        break
                                elif method in methodList:
                                    for client in clients.values():
                                        client.send(res)
                                else:
                                    """In case if multiplying is not necessary, just resending message from server"""
                                    dest = clients[key.fileobj]
                                    dest.send(res)
                            """Checking if message comes from client"""
                            if key.fileobj in clients.values():
                                for client in clients.keys():
                                    if clients[client] == key.fileobj:
                                        client.send(res)
                                        break
                    """In case if there is no such socket"""
                    """Deleting sockets and unregistering them from listening"""
                except:
                    try:
                        """Case when it's client's socket"""
                        if key.fileobj in clients.keys():
                            tmp = clients[key.fileobj]
                            s.unregister(key.fileobj)
                            s.unregister(tmp)
                            tmp.close()
                            key.fileobj.close()
                            clients.pop(key.fileobj)

                            break
                        """Case when it's server's socket"""
                        if key.fileobj in clients.values():
                            for client in clients.keys():
                                if clients[client] == key.fileobj:
                                    s.unregister(client)
                                    s.unregister(key.fileobj)
                                    client.close()
                                    key.fileobj.close()
                                    clients.pop(client)
                                    break

                    except:
                        pass
    except:
        pass
    pass


def queue(sock=None, conn=None):
    with lock:
        """Registering sockets for listening"""
        if not sock and not conn:
            try:
                for i in unregClients.keys():
                    s.register(i, selectors.EVENT_READ)
                for j in unregClients.values():
                    s.register(j, selectors.EVENT_READ)
                unregClients.clear()
            except:
                pass
            """Adding just-connected sockets"""
        else:
            try:
                unregClients[sock] = conn
                clients[sock] = conn
            except:
                pass
    pass


def newConnection():
    try:
        tcConn, tcAddr = toClient.accept()
        sock = socket.socket()
        sock.connect((r[0], int(r[1])))
        queue(sock, tcConn)
    except:
        pass


try:
    while True:
        try:
            t1 = Thread(target=newConnection()).start()
            if len(clients) > 0:
                t2 = Thread(target=listener()).start()
        except:
        	pass
finally:
    toClient.close()
